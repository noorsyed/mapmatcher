﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MapMatcher.Util
{
    public class DistanceFinder
    {
        public static double GetDistanceFromLatLonInMeters(double lat1, double lng1, double lat2, double lng2)
        {
            var R = 6371; 
            var dLat = Deg2Rad(lat2 - lat1);  
            var dLon = Deg2Rad(lng2 - lng1);
            var a =
              Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
              Math.Cos(Deg2Rad(lat1)) * Math.Cos(Deg2Rad(lat2)) *
              Math.Sin(dLon / 2) * Math.Sin(dLon / 2)
              ;
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = R * c; 
            return d * 1000;
        }

        private static double Deg2Rad(double deg) => deg * (Math.PI / 180);
    }
}