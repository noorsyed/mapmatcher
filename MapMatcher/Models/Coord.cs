﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MapMatcher.Models
{
    public class Coord
    {
        public int id { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public string normalizedAddress { get; set; }
        public int orderId { get; set; }
        public List<AddressCord> address { get; set; }

    }

    public class AddressCord
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
}