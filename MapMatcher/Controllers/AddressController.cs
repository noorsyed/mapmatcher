﻿using MapMatcher.Models;
using static MapMatcher.Util.DistanceFinder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MapMatcher.Controllers
{

    public class addressmodel
    {
        public decimal Id { get; set; }
        public string Name { get; set; }

        public string Address { get; set; }


    }

    public class AddressController : ApiController
    {

        public IHttpActionResult Get(int Id)
        {

            var pagerResult = new Pager();
            int take = 10;
            int segment = 3;
            int? pageId = Id;// Convert.ToInt32(p[2]);
            int skip = (Convert.ToInt32(pageId) - 1) * take;

            var remainGoogleAddress = new List<addressmodel>();
            var remainBingAddress = new List<addressmodel>();


            var addressList = new List<addressmodel>();
            try
            {
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {

                    int rowCount = datacontext.ADDRESSES.Count();

                    // var lstAddress = datacontext.ADDRESSES.OrderBy(x=>x.ID_ADDRESS).ToList().Skip(skip).Take(take);
                    //var lstAddress = datacontext.ADDRESSES.OrderBy(x => x.ID_ADDRESS).ToList()..Take(take);

                    var lstAddress = (from address in datacontext.ADDRESSES.OrderBy(x => x.ID_ADDRESS).ToList()
                                      join addressresult in datacontext.ADDRESS_RESULT_COUNT.OrderBy(x => x.ID_ADDRESS).ToList()
                                      on address.ID_ADDRESS equals addressresult.ID_ADDRESS
                                      into grp
                                      from addr in grp.DefaultIfEmpty<ADDRESS_RESULT_COUNT>(
                                          new ADDRESS_RESULT_COUNT
                                          {
                                              ID_ADDRESS = 0
                                          })
                                      where addr.ID_ADDRESS == 0
                                      select new addressmodel
                                      {
                                          Id = address.ID_ADDRESS,
                                          Name = address.ADDRESS_FIELD01,
                                          Address = address.ADDRESS_FIELD01 + "," + address.ADDRESS_FIELD02 + "," + address.ADDRESS_FIELD03 + "," + address.ADDRESS_FIELD04 + "," + address.ADDRESS_FIELD05 + "," + address.ADDRESS_FIELD06 + "," + address.ADDRESS_FIELD07,

                                      }).ToList().Take(take);



                    //foreach (var addr in lstAddress)
                    //{
                    //    addressList.Add(new addressmodel
                    //    {
                    //        Id = addr.ID_ADDRESS,
                    //        Name = addr.ADDRESS_FIELD01,
                    //        Address = addr.ADDRESS_FIELD01 + "," + addr.ADDRESS_FIELD02 + "," + addr.ADDRESS_FIELD03 + "," + addr.ADDRESS_FIELD04 + "," + addr.ADDRESS_FIELD05 + "," + addr.ADDRESS_FIELD06 + "," + addr.ADDRESS_FIELD07,
                    //    });
                    //}

                    pagerResult = Pager.Items(rowCount).PerPage(take).Move(pageId ?? 1).Segment(segment).Center();

                    PagerValue pagerValue = new PagerValue();
                    pagerValue.CurrentPageIndex = pagerResult.CurrentPageIndex;
                    pagerValue.FirstPageIndex = pagerResult.FirstPageIndex;
                    pagerValue.HasNextPage = pagerResult.HasNextPage;
                    pagerValue.HasPreviousPage = pagerResult.HasPreviousPage;
                    pagerValue.LastPageIndex = pagerResult.LastPageIndex;
                    pagerValue.NextPageIndex = pagerResult.NextPageIndex;
                    pagerValue.NumberOfPages = pagerResult.NumberOfPages;
                    pagerValue.PreviousPageIndex = pagerResult.PreviousPageIndex;
                   // Tuple<List<PagerValue>, List<addressmodel>> data = new Tuple<List<PagerValue>, List<addressmodel>>(new List<PagerValue> { pagerValue }, lstAddress.ToList());
                    // return this.Request.CreateResponse(HttpStatusCode.OK, data);

                    //return Ok(addressList);

                    //if(pageId==1)
                    //{
                        remainGoogleAddress = (from add in datacontext.ADDRESSES
                                               join gadd in datacontext.GOOGLEMAPS_RESULTS
                                              on add.ID_ADDRESS equals gadd.ID_ADDRESS
                                               where
                                               gadd.LATTITUDE == -1 || gadd.LONGITUD == -1
                                               select new addressmodel
                                               {
                                                   Id = add.ID_ADDRESS,
                                                   Name = add.ADDRESS_FIELD01,
                                                   Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                               }).Distinct().ToList();

                        remainBingAddress = (from add in datacontext.ADDRESSES
                                             join badd in datacontext.BINGMAPS_RESULTS
                                            on add.ID_ADDRESS equals badd.ID_ADDRESS
                                             where
                                             badd.LATTITUDE == -1 || badd.LONGITUD == -1
                                             select new addressmodel
                                             {
                                                 Id = add.ID_ADDRESS,
                                                 Name = add.ADDRESS_FIELD01,
                                                 Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                             }).Distinct().ToList();
                   // }

                    Tuple<List<PagerValue>, List<addressmodel>, List<addressmodel>, List<addressmodel>> data = new Tuple<List<PagerValue>, List<addressmodel>, List<addressmodel>, List<addressmodel>>(new List<PagerValue> { pagerValue }, lstAddress.ToList(), remainGoogleAddress.ToList(),remainBingAddress.ToList());
                    return Ok(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpDelete]
        public IHttpActionResult Delete()
        {
            try
            {
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    var existingAddressResultCounts = datacontext.ADDRESS_RESULT_COUNT.ToList();
                    datacontext.ADDRESS_RESULT_COUNT.RemoveRange(existingAddressResultCounts);

                    var existingBingCoords = datacontext.BINGMAPS_RESULTS.ToList();
                    datacontext.BINGMAPS_RESULTS.RemoveRange(existingBingCoords);

                    var existingGoogleCoords = datacontext.GOOGLEMAPS_RESULTS.ToList();
                    datacontext.GOOGLEMAPS_RESULTS.RemoveRange(existingGoogleCoords);

                    var existingcoordinatesComparison = datacontext.COORDINATES_COMPARISON.ToList();
                    datacontext.COORDINATES_COMPARISON.RemoveRange(existingcoordinatesComparison);

                    datacontext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('BINGMAPS_RESULTS',RESEED,0);");
                    datacontext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('GOOGLEMAPS_RESULTS',RESEED,0);");


                    datacontext.SaveChanges();
                }
                return Ok();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //[HttpPost]
        //public IHttpActionResult PostGoogleAddress(dynamic coords)
        //{
        //    int resultcount = 0;
        //    return Ok(resultcount);
        //}
        [HttpPost]
        public IHttpActionResult Post(dynamic coords)
        {
            int resultcount = 0;
            var failedGoogleAddresses = new List<addressmodel>();
            var failedBingAddresses = new List<addressmodel>();
            try
            {
                var bingCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.bing.Value);
                var googleCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.google.Value);
                List<GOOGLEMAPS_RESULTS> lstGoogleMapResult = new List<GOOGLEMAPS_RESULTS>();
                List<BINGMAPS_RESULTS> lstBingMapResult = new List<BINGMAPS_RESULTS>();

                short resultOrder = 0;
                int take = 10;
                var pageId = coords.pageId.Value;
                var hasNextPage = coords.nextPage.Value;
                int skip = (Convert.ToInt32(pageId) - 1) * take;
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    //resultOrder = datacontext.BINGMAPS_RESULTS.Max(e => (short?)e.BING_RESULT_ORDER) ?? 0;
                    resultOrder = datacontext.BINGMAPS_RESULTS.Max(e => (short?)e.BingId) ?? 0;
                    foreach (var item in bingCoords)
                    {
                        BINGMAPS_RESULTS bingmapResult = new BINGMAPS_RESULTS();
                        bingmapResult.ID_ADDRESS = item.id;
                        bingmapResult.LATTITUDE = item.lat;
                        bingmapResult.LONGITUD = item.lng;
                        //bingmapResult.BING_RESULT_ORDER = ++resultOrder;
                        bingmapResult.BING_RESULT_ORDER = (short)item.orderId;
                        bingmapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                        lstBingMapResult.Add(bingmapResult);

                    }
                    // lstBingMapResult.ForEach(x => x.BING_RESULT_ORDER = ++resultOrder);

                    var bingQuery = (from adr in datacontext.ADDRESSES
                                     .OrderBy(x => x.ID_ADDRESS)
                                     .ToList()
                                     .Skip(skip)
                                     .Take(take)
                                     join gmr in lstBingMapResult.Distinct()
                                     on adr.ID_ADDRESS equals gmr.ID_ADDRESS
                                     into grp
                                     from gmr in grp.DefaultIfEmpty<BINGMAPS_RESULTS>(
                                         new BINGMAPS_RESULTS
                                         {
                                             LATTITUDE = -1,
                                             LONGITUD = -1,
                                             NORMALIZED_ADDRESS = string.Empty,
                                             BING_RESULT_ORDER=0
                                         })
                                     where gmr.LATTITUDE == -1
                                     select new BINGMAPS_RESULTS
                                     {
                                         ID_ADDRESS = adr.ID_ADDRESS,
                                         LATTITUDE = gmr.LATTITUDE,
                                         LONGITUD = gmr.LONGITUD,
                                         NORMALIZED_ADDRESS = gmr.NORMALIZED_ADDRESS,
                                         BING_RESULT_ORDER=gmr.BING_RESULT_ORDER

                                     }).ToList();
                    //bingQuery.ForEach(x => x.BING_RESULT_ORDER = ++resultOrder);
                    lstBingMapResult.AddRange(bingQuery);


                    lstBingMapResult = (from bmr in lstBingMapResult.ToList()
                                        join addressresult in datacontext.ADDRESS_RESULT_COUNT.ToList()
                                        on bmr.ID_ADDRESS equals addressresult.ID_ADDRESS
                                        into grp
                                        from addr in grp.DefaultIfEmpty<ADDRESS_RESULT_COUNT>(
                                            new ADDRESS_RESULT_COUNT
                                            {
                                                ID_ADDRESS = 0
                                            })
                                        where addr.ID_ADDRESS == 0
                                        select new BINGMAPS_RESULTS
                                        {
                                            ID_ADDRESS = bmr.ID_ADDRESS,
                                            LATTITUDE = bmr.LATTITUDE,
                                            LONGITUD = bmr.LONGITUD,
                                            NORMALIZED_ADDRESS = bmr.NORMALIZED_ADDRESS,
                                            BING_RESULT_ORDER=bmr.BING_RESULT_ORDER
                                        }).ToList();
                    // lstBingMapResult.ForEach(x => x.BING_RESULT_ORDER = ++resultOrder);
                    lstBingMapResult.ForEach(x => x.BingId = ++resultOrder);

                    resultOrder = 0;
                    resultOrder = datacontext.GOOGLEMAPS_RESULTS.Max(e => (short?)e.GoogleId) ?? 0;

                    foreach (var item in googleCoords)
                    {
                        GOOGLEMAPS_RESULTS googleMapResult = new GOOGLEMAPS_RESULTS();
                        googleMapResult.ID_ADDRESS = item.id;
                        googleMapResult.LATTITUDE = item.lat;
                        googleMapResult.LONGITUD = item.lng;
                        // googleMapResult.GOOGLE_RESULT_ORDER = ++resultOrder;
                        googleMapResult.GOOGLE_RESULT_ORDER = (short)item.orderId;
                        googleMapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                        for (int i = 0; i < item.address.Count; i++)
                        {
                            if (i == 0)
                                googleMapResult.ADDRESS_SEGMENT_01 = item.address[i].long_name;
                            else if (i == 1)
                                googleMapResult.ADDRESS_SEGMENT_02 = item.address[i].long_name;
                            else if (i == 2)
                                googleMapResult.ADDRESS_SEGMENT_03 = item.address[i].long_name;
                            else if (i == 3)
                                googleMapResult.ADDRESS_SEGMENT_04 = item.address[i].long_name;
                            else if (i == 4)
                                googleMapResult.ADDRESS_SEGMENT_05 = item.address[i].long_name;
                            else if (i == 5)
                                googleMapResult.ADDRESS_SEGMENT_06 = item.address[i].long_name;
                            else if (i == 6)
                                googleMapResult.ADDRESS_SEGMENT_07 = item.address[i].long_name;
                            else if (i == 7)
                                googleMapResult.ADDRESS_SEGMENT_08 = item.address[i].long_name;
                            else if (i == 8)
                                googleMapResult.ADDRESS_SEGMENT_09 = item.address[i].long_name;
                            else if (i == 9)
                                googleMapResult.ADDRESS_SEGMENT_10 = item.address[i].long_name;

                        }

                        lstGoogleMapResult.Add(googleMapResult);

                    }
                    // lstGoogleMapResult.ForEach(x => x.GOOGLE_RESULT_ORDER = ++resultOrder);

                    var googleQuery = (from adr in datacontext.ADDRESSES.OrderBy(x => x.ID_ADDRESS).ToList().Skip(skip).Take(take)
                                       join gmr in lstGoogleMapResult.Distinct()
                                       on adr.ID_ADDRESS equals gmr.ID_ADDRESS
                                       into grp
                                       from gmr in grp.DefaultIfEmpty<GOOGLEMAPS_RESULTS>(
                                           new GOOGLEMAPS_RESULTS
                                           {
                                               LATTITUDE = -1,
                                               LONGITUD = -1,
                                               NORMALIZED_ADDRESS = string.Empty,
                                               GOOGLE_RESULT_ORDER=0
                                           })
                                       where gmr.LATTITUDE == -1
                                       select new GOOGLEMAPS_RESULTS
                                       {
                                           ID_ADDRESS = adr.ID_ADDRESS,
                                           LATTITUDE = gmr.LATTITUDE,
                                           LONGITUD = gmr.LONGITUD,
                                           NORMALIZED_ADDRESS = gmr.NORMALIZED_ADDRESS,
                                           ADDRESS_SEGMENT_01 = gmr.ADDRESS_SEGMENT_01,
                                           ADDRESS_SEGMENT_02 = gmr.ADDRESS_SEGMENT_02,
                                           ADDRESS_SEGMENT_03 = gmr.ADDRESS_SEGMENT_03,
                                           ADDRESS_SEGMENT_04 = gmr.ADDRESS_SEGMENT_04,
                                           ADDRESS_SEGMENT_05 = gmr.ADDRESS_SEGMENT_05,
                                           ADDRESS_SEGMENT_06 = gmr.ADDRESS_SEGMENT_06,
                                           ADDRESS_SEGMENT_07 = gmr.ADDRESS_SEGMENT_07,
                                           ADDRESS_SEGMENT_08 = gmr.ADDRESS_SEGMENT_08,
                                           ADDRESS_SEGMENT_09 = gmr.ADDRESS_SEGMENT_09,
                                           ADDRESS_SEGMENT_10 = gmr.ADDRESS_SEGMENT_10,
                                           GOOGLE_RESULT_ORDER=gmr.GOOGLE_RESULT_ORDER
                                       }).ToList();
                    // googleQuery.ForEach(x => x.GOOGLE_RESULT_ORDER = ++resultOrder);
                    lstGoogleMapResult.AddRange(googleQuery);


                    lstGoogleMapResult = (from gmr in lstGoogleMapResult.ToList()
                                          join addressresult in datacontext.ADDRESS_RESULT_COUNT.ToList()
                                          on gmr.ID_ADDRESS equals addressresult.ID_ADDRESS
                                          into grp
                                          from addr in grp.DefaultIfEmpty<ADDRESS_RESULT_COUNT>(
                                              new ADDRESS_RESULT_COUNT
                                              {
                                                  ID_ADDRESS = 0
                                              })
                                          where addr.ID_ADDRESS == 0
                                          select new GOOGLEMAPS_RESULTS
                                          {
                                              ID_ADDRESS = gmr.ID_ADDRESS,
                                              LATTITUDE = gmr.LATTITUDE,
                                              LONGITUD = gmr.LONGITUD,
                                              NORMALIZED_ADDRESS = gmr.NORMALIZED_ADDRESS,
                                              ADDRESS_SEGMENT_01 = gmr.ADDRESS_SEGMENT_01,
                                              ADDRESS_SEGMENT_02 = gmr.ADDRESS_SEGMENT_02,
                                              ADDRESS_SEGMENT_03 = gmr.ADDRESS_SEGMENT_03,
                                              ADDRESS_SEGMENT_04 = gmr.ADDRESS_SEGMENT_04,
                                              ADDRESS_SEGMENT_05 = gmr.ADDRESS_SEGMENT_05,
                                              ADDRESS_SEGMENT_06 = gmr.ADDRESS_SEGMENT_06,
                                              ADDRESS_SEGMENT_07 = gmr.ADDRESS_SEGMENT_07,
                                              ADDRESS_SEGMENT_08 = gmr.ADDRESS_SEGMENT_08,
                                              ADDRESS_SEGMENT_09 = gmr.ADDRESS_SEGMENT_09,
                                              ADDRESS_SEGMENT_10 = gmr.ADDRESS_SEGMENT_10,
                                              GOOGLE_RESULT_ORDER = gmr.GOOGLE_RESULT_ORDER
                                          }).ToList();
                    //lstGoogleMapResult.ForEach(x => x.GOOGLE_RESULT_ORDER = ++resultOrder);
                    lstGoogleMapResult.ForEach(x => x.GoogleId = ++resultOrder);



                    var query = from gmr in lstGoogleMapResult
                                from bmr in lstBingMapResult
                                where gmr.ID_ADDRESS == bmr.ID_ADDRESS
                                orderby gmr.ID_ADDRESS

                                select new
                                {
                                    AddressId = gmr.ID_ADDRESS,
                                    GoogleLatitude = gmr.LATTITUDE,
                                    GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
                                    GoogleLongitude = gmr.LONGITUD,
                                    BingLatitude = bmr.LATTITUDE,
                                    BingLongitude = bmr.LONGITUD,
                                    BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
                                    BingResultOrder = bmr.BING_RESULT_ORDER,
                                    GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER,
                                    BingId= bmr.BingId,
                                    GoogleId=gmr.GoogleId
                                };



                    int mapResultCount = query.Count();


                    List<COORDINATES_COMPARISON> lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();

                    foreach (var res in query)
                    {


                        COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
                        coordinatesComparison.ID_ADDRESS = res.AddressId;
                        coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
                        coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
                        coordinatesComparison.GoogleId = res.GoogleId;
                        coordinatesComparison.BingId = res.BingId;
                        coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == -1 || res.BingLongitude == -1 || res.GoogleLatitude == -1 || res.GoogleLongitude == -1) ? -1 :
                            (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
                        lstCoordinatesComparison.Add(coordinatesComparison);
                    }


                    var bingResultCount = from bing in lstBingMapResult
                                          where bing.LATTITUDE != -1 || bing.LONGITUD != -1
                                          orderby bing.ID_ADDRESS
                                          group bing by bing.ID_ADDRESS into grp

                                          select new { AddressId = grp.Key, ResultCount = grp.Count() };


                    var bingResultCountMissing = from bing in lstBingMapResult
                                                 where bing.LATTITUDE == -1 || bing.LONGITUD == -1
                                                 orderby bing.ID_ADDRESS
                                                 group bing by bing.ID_ADDRESS into grp

                                                 select new { AddressId = grp.Key, ResultCount = 0 };


                    var googleResultCount = from google in lstGoogleMapResult
                                            where google.LATTITUDE != -1 || google.LONGITUD != -1
                                            orderby google.ID_ADDRESS
                                            group google by google.ID_ADDRESS into grp
                                            select new { AddressId = grp.Key, ResultCount = grp.Count() };

                    var googleResultCountMissing = from google in lstGoogleMapResult
                                            where google.LATTITUDE == -1 || google.LONGITUD == -1
                                            orderby google.ID_ADDRESS
                                            group google by google.ID_ADDRESS into grp
                                            select new { AddressId = grp.Key, ResultCount =0 };
                   

                    var addressResultList = from b in bingResultCount.Union(bingResultCountMissing)
                                            join g in googleResultCount.Union(googleResultCountMissing)
                                            on b.AddressId equals g.AddressId
                                            select new ADDRESS_RESULT_COUNT
                                            {
                                                ID_ADDRESS = b.AddressId,
                                                GOOGLE_RESULT_COUNT = g.ResultCount,
                                                BING_RESULT_COUNT = b.ResultCount
                                            };





                    //var existingAddressResultCounts = datacontext.AddressResultCounts.ToList();
                    //datacontext.AddressResultCounts.RemoveRange(existingAddressResultCounts);

                    //var existingBingCoords = datacontext.BINGMAPS_RESULTS.ToList();
                    //datacontext.BINGMAPS_RESULTS.RemoveRange(existingBingCoords);

                    //var existingGoogleCoords = datacontext.GOOGLEMAPS_RESULTS.ToList();
                    //datacontext.GOOGLEMAPS_RESULTS.RemoveRange(existingGoogleCoords);

                    //var existingcoordinatesComparison = datacontext.COORDINATES_COMPARISON.ToList();
                    //datacontext.COORDINATES_COMPARISON.RemoveRange(existingcoordinatesComparison);


                    datacontext.ADDRESS_RESULT_COUNT.AddRange(addressResultList);
                    datacontext.BINGMAPS_RESULTS.AddRange(lstBingMapResult);
                    datacontext.GOOGLEMAPS_RESULTS.AddRange(lstGoogleMapResult);


                    datacontext.COORDINATES_COMPARISON.AddRange(lstCoordinatesComparison);
                    datacontext.SaveChanges();


                    resultcount = datacontext.ADDRESS_RESULT_COUNT.Count();


                   //if(resultcount ==98)// if (!hasNextPage)
                   // {
                        failedGoogleAddresses = (from add in datacontext.ADDRESSES
                                               join gadd in datacontext.GOOGLEMAPS_RESULTS
                                              on add.ID_ADDRESS equals gadd.ID_ADDRESS
                                               where
                                               gadd.LATTITUDE == -1 || gadd.LONGITUD == -1
                                               select new addressmodel
                                               {
                                                   Id = add.ID_ADDRESS,
                                                   Name = add.ADDRESS_FIELD01,
                                                   Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                               }).Distinct().ToList();

                        failedBingAddresses = (from add in datacontext.ADDRESSES
                                               join badd in datacontext.BINGMAPS_RESULTS
                                              on add.ID_ADDRESS equals badd.ID_ADDRESS
                                               where
                                               badd.LATTITUDE == -1 || badd.LONGITUD == -1
                                               select new addressmodel
                                               {
                                                   Id = add.ID_ADDRESS,
                                                   Name = add.ADDRESS_FIELD01,
                                                   Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                               }).Distinct().ToList();
                      

                        //remainBingAddress = (from add in datacontext.ADDRESSES
                        //                     where (from badd in datacontext.BINGMAPS_RESULTS
                        //                            where (badd.LATTITUDE == -1 || badd.LONGITUD == -1)
                        //                            select badd.ID_ADDRESS)
                        //                            .Contains(add.ID_ADDRESS)
                        //                     select new addressmodel
                        //                     {
                        //                         Id = add.ID_ADDRESS,
                        //                         Name = add.ADDRESS_FIELD01,
                        //                         Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                        //                     }).Distinct().ToList();

                   // }

                }

                Tuple<int, List<addressmodel>, List<addressmodel>> data = new Tuple<int, List<addressmodel>, List<addressmodel>>(resultcount, failedGoogleAddresses.ToList(), failedBingAddresses.ToList());

                //return Ok(resultcount);
                return Ok(data);
            }
            catch (Exception ex)
            {
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    resultcount = datacontext.ADDRESS_RESULT_COUNT.Count();
                }
                throw new InvalidOperationException(resultcount.ToString());


            }
        }


        //public IHttpActionResult Post(dynamic coords)
        //{
        //    try
        //    {
        //        var bingCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.bing.Value);
        //        var googleCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.google.Value);
        //        List<GOOGLEMAPS_RESULTS> lstGoogleMapResult = new List<GOOGLEMAPS_RESULTS>();
        //        List<BINGMAPS_RESULTS> lstBingMapResult = new List<BINGMAPS_RESULTS>();

        //        short resultOrder = 0;


        //        using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
        //        {
        //            foreach (var item in bingCoords)
        //            {
        //                BINGMAPS_RESULTS bingmapResult = new BINGMAPS_RESULTS();
        //                bingmapResult.ID_ADDRESS = item.id;
        //                bingmapResult.LATTITUDE = item.lat;
        //                bingmapResult.LONGITUD = item.lng;
        //                //bingmapResult.BING_RESULT_ORDER = ++resultOrder;
        //                bingmapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
        //                lstBingMapResult.Add(bingmapResult);

        //            }
        //            lstBingMapResult.ForEach(x => x.BING_RESULT_ORDER = ++resultOrder);

        //            var bingQuery = (from adr in datacontext.ADDRESSES.ToList()
        //                             join gmr in lstBingMapResult.Distinct()
        //                             on adr.ID_ADDRESS equals gmr.ID_ADDRESS
        //                             into grp
        //                             from gmr in grp.DefaultIfEmpty<BINGMAPS_RESULTS>(
        //                                 new BINGMAPS_RESULTS
        //                                 {
        //                                     LATTITUDE = 0,
        //                                     LONGITUD = 0,
        //                                     NORMALIZED_ADDRESS = string.Empty
        //                                 })
        //                             where gmr.LATTITUDE == 0
        //                             select new BINGMAPS_RESULTS
        //                             {
        //                                 ID_ADDRESS = adr.ID_ADDRESS,
        //                                 LATTITUDE = gmr.LATTITUDE,
        //                                 LONGITUD = gmr.LONGITUD,
        //                                 NORMALIZED_ADDRESS = gmr.NORMALIZED_ADDRESS
        //                             }).ToList();
        //            bingQuery.ForEach(x => x.BING_RESULT_ORDER = ++resultOrder);
        //            lstBingMapResult.AddRange(bingQuery);

        //            resultOrder = 0;

        //            foreach (var item in googleCoords)
        //            {
        //                GOOGLEMAPS_RESULTS googleMapResult = new GOOGLEMAPS_RESULTS();
        //                googleMapResult.ID_ADDRESS = item.id;
        //                googleMapResult.LATTITUDE = item.lat;
        //                googleMapResult.LONGITUD = item.lng;
        //               // googleMapResult.GOOGLE_RESULT_ORDER = ++resultOrder;
        //                googleMapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
        //                lstGoogleMapResult.Add(googleMapResult);

        //            }
        //            lstGoogleMapResult.ForEach(x => x.GOOGLE_RESULT_ORDER = ++resultOrder);

        //            var googleQuery = (from adr in datacontext.ADDRESSES.ToList()
        //                               join gmr in lstGoogleMapResult.Distinct()
        //                               on adr.ID_ADDRESS equals gmr.ID_ADDRESS
        //                               into grp
        //                               from gmr in grp.DefaultIfEmpty<GOOGLEMAPS_RESULTS>(
        //                                   new GOOGLEMAPS_RESULTS
        //                                   {
        //                                       LATTITUDE = 0,
        //                                       LONGITUD = 0,
        //                                       NORMALIZED_ADDRESS = string.Empty
        //                                   })
        //                               where gmr.LATTITUDE == 0
        //                               select new GOOGLEMAPS_RESULTS
        //                               {
        //                                   ID_ADDRESS = adr.ID_ADDRESS,
        //                                   LATTITUDE = gmr.LATTITUDE,
        //                                   LONGITUD = gmr.LONGITUD,
        //                                   NORMALIZED_ADDRESS = gmr.NORMALIZED_ADDRESS
        //                               }).ToList();
        //            googleQuery.ForEach(x => x.GOOGLE_RESULT_ORDER = ++resultOrder);
        //            lstGoogleMapResult.AddRange(googleQuery);



        //            var query = from gmr in lstGoogleMapResult
        //                        from bmr in lstBingMapResult
        //                        where gmr.ID_ADDRESS==bmr.ID_ADDRESS
        //                        orderby gmr.ID_ADDRESS

        //                        select new
        //                        {
        //                            AddressId = gmr.ID_ADDRESS,
        //                            GoogleLatitude = gmr.LATTITUDE,
        //                            GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
        //                            GoogleLongitude = gmr.LONGITUD,
        //                            BingLatitude = bmr.LATTITUDE,
        //                            BingLongitude = bmr.LONGITUD,
        //                            BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
        //                            BingResultOrder = bmr.BING_RESULT_ORDER,
        //                            GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER
        //                        };


        //            int mapResultCount = query.Count();


        //            List<COORDINATES_COMPARISON> lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();

        //            foreach (var res in query)
        //            {


        //                COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
        //                coordinatesComparison.ID_ADDRESS = res.AddressId;
        //                coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
        //                coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
        //                coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == 0 || res.BingLongitude == 0 || res.GoogleLatitude == 0 || res.GoogleLongitude == 0) ? 0 :
        //                    (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
        //                lstCoordinatesComparison.Add(coordinatesComparison);
        //            }


        //            var bingResultCount = from bing in lstBingMapResult
        //                                  orderby bing.ID_ADDRESS
        //                                  group bing by bing.ID_ADDRESS into grp
        //                                  select new { AddressId = grp.Key, ResultCount = grp.Count() };

        //            var googleResultCount = from google in lstGoogleMapResult
        //                                    orderby google.ID_ADDRESS
        //                                    group google by google.ID_ADDRESS into grp
        //                                    select new { AddressId = grp.Key, ResultCount = grp.Count() };

        //            var addressResultList = from b in bingResultCount
        //                                    join g in googleResultCount
        //                                    on b.AddressId equals g.AddressId
        //                                    select new AddressResultCount
        //                                    {
        //                                        AddressId = b.AddressId,
        //                                        GoogleResultCount = g.ResultCount,
        //                                        BingResultCount = b.ResultCount
        //                                    };

        //            //datacontext.AddressResultCounts.ToList().RemoveAll(x => true);
        //            //datacontext.BINGMAPS_RESULTS.ToList().RemoveAll(x => true);
        //            //datacontext.GOOGLEMAPS_RESULTS.ToList().RemoveAll(x => true);
        //            //datacontext.COORDINATES_COMPARISON.ToList().RemoveAll(x => true);

        //            var existingAddressResultCounts = datacontext.AddressResultCounts.ToList();
        //            datacontext.AddressResultCounts.RemoveRange(existingAddressResultCounts);

        //            var existingBingCoords = datacontext.BINGMAPS_RESULTS.ToList();
        //            datacontext.BINGMAPS_RESULTS.RemoveRange(existingBingCoords);

        //            var existingGoogleCoords = datacontext.GOOGLEMAPS_RESULTS.ToList();
        //            datacontext.GOOGLEMAPS_RESULTS.RemoveRange(existingGoogleCoords);

        //            var existingcoordinatesComparison = datacontext.COORDINATES_COMPARISON.ToList();
        //            datacontext.COORDINATES_COMPARISON.RemoveRange(existingcoordinatesComparison);


        //            datacontext.AddressResultCounts.AddRange(addressResultList);
        //            datacontext.BINGMAPS_RESULTS.AddRange(lstBingMapResult);
        //            datacontext.GOOGLEMAPS_RESULTS.AddRange(lstGoogleMapResult);

        //            //datacontext.SaveChanges();

        //            //foreach(var item in lstCoordinatesComparison)
        //            //{
        //            //    try {
        //            //        datacontext.COORDINATES_COMPARISON.Add(item);
        //            //        datacontext.SaveChanges();
        //            //    }
        //            //    catch (Exception e1)
        //            //    {
        //            //        throw e1;
        //            //    }


        //            //}
        //            datacontext.COORDINATES_COMPARISON.AddRange(lstCoordinatesComparison);
        //            datacontext.SaveChanges();


        //        }
        //        return Ok();
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}





    }





}
