﻿using MapMatcher.Models;
using Newtonsoft.Json;
using static MapMatcher.Util.DistanceFinder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MapMatcher.Controllers
{
    public class comparisonController : ApiController
    {


        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult Post(dynamic coords)
        {

            try
            {
                var lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();

                var bingCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.bing.Value);
                var googleCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.google.Value);
                List<GOOGLEMAPS_RESULTS> lstGoogleMapResult = new List<GOOGLEMAPS_RESULTS>();
                List<BINGMAPS_RESULTS> lstBingMapResult = new List<BINGMAPS_RESULTS>();


                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    //existingcoordinatesComparison = await datacontext.COORDINATES_COMPARISON
                    //                                         .Where(x => x.ID_ADDRESS == id)
                    //                                         .ToListAsync();

                    foreach (var item in bingCoords)
                    {
                        BINGMAPS_RESULTS bingmapResult = new BINGMAPS_RESULTS();
                        bingmapResult.ID_ADDRESS = item.id;
                        bingmapResult.LATTITUDE = item.lat;
                        bingmapResult.LONGITUD = item.lng;
                        //bingmapResult.BING_RESULT_ORDER = ++resultOrder;
                        bingmapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                        lstBingMapResult.Add(bingmapResult);

                    }

                    foreach (var item in googleCoords)
                    {
                        GOOGLEMAPS_RESULTS googleMapResult = new GOOGLEMAPS_RESULTS();
                        googleMapResult.ID_ADDRESS = item.id;
                        googleMapResult.LATTITUDE = item.lat;
                        googleMapResult.LONGITUD = item.lng;
                        // googleMapResult.GOOGLE_RESULT_ORDER = ++resultOrder;
                        googleMapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                        lstGoogleMapResult.Add(googleMapResult);

                    }

                    var query = from gmr in lstGoogleMapResult
                                from bmr in lstBingMapResult
                               // where gmr.ID_ADDRESS == bmr.ID_ADDRESS //&& (gmr.ID_ADDRESS == 1 || bmr.ID_ADDRESS == 1)
                                orderby gmr.ID_ADDRESS

                                select new
                                {
                                    AddressId = gmr.ID_ADDRESS,
                                    GoogleLatitude = gmr.LATTITUDE,
                                    GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
                                    GoogleLongitude = gmr.LONGITUD,
                                    BingLatitude = bmr.LATTITUDE,
                                    BingLongitude = bmr.LONGITUD,
                                    BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
                                    BingResultOrder = bmr.BING_RESULT_ORDER,
                                    GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER,
                                    DistanceBetweenpointsMeters = (bmr.LATTITUDE == -1 || bmr.LONGITUD == -1 || gmr.LATTITUDE == -1 || gmr.LONGITUD == -1) ? -1 :
                                           (decimal)GetDistanceFromLatLonInMeters((double)gmr.LATTITUDE, (double)gmr.LONGITUD, (double)bmr.LATTITUDE, (double)bmr.LONGITUD)
                                };

                  //lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();

                  //  foreach (var res in query)
                  //  {


                  //      COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
                  //      coordinatesComparison.ID_ADDRESS = res.AddressId;
                  //      coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
                  //      coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
                  //      coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == -1 || res.BingLongitude == -1 || res.GoogleLatitude == -1 || res.GoogleLongitude == -1) ? -1 :
                  //          (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
                  //      lstCoordinatesComparison.Add(coordinatesComparison);
                  //  }

                    return Ok(query);


                }

            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}