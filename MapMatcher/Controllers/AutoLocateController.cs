﻿using MapMatcher.Models;
using Newtonsoft.Json;
using static MapMatcher.Util.DistanceFinder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MapMatcher.Controllers
{
    public class LocateController : ApiController
    {
        [HttpGet]
        public async Task<IHttpActionResult> Get(string id)
        {
            var addressList = new List<addressmodel>();
            try
            {
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    var lstAddress = await (from address in datacontext.ADDRESSES
                                            select new addressmodel
                                            {
                                                Id = address.ID_ADDRESS,
                                                Name = address.ADDRESS_FIELD01,
                                                Address = address.ADDRESS_FIELD01 + "," + address.ADDRESS_FIELD02 + "," + address.ADDRESS_FIELD03 + "," + address.ADDRESS_FIELD04 + "," + address.ADDRESS_FIELD05 + "," + address.ADDRESS_FIELD06 + "," + address.ADDRESS_FIELD07,

                                            })
                                     .Where(x => x.Address.Contains(id))
                                    .OrderBy(x => x.Id)
                                    .Take(10)
                                    .ToListAsync();
                    return Ok(lstAddress);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public IHttpActionResult Post(dynamic coords)
        {
            int resultcount = 0;
            var failedGoogleAddresses = new List<addressmodel>();
            
            try
            {
                var googleCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.google.Value);
                List<GOOGLEMAPS_RESULTS> lstGoogleMapResult = new List<GOOGLEMAPS_RESULTS>();
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    foreach (var item in googleCoords)
                    {
                        if (item.id != 0)
                        {
                            GOOGLEMAPS_RESULTS googleMapResult = new GOOGLEMAPS_RESULTS();
                            googleMapResult.ID_ADDRESS = item.id;
                            googleMapResult.LATTITUDE = item.lat;
                            googleMapResult.LONGITUD = item.lng;
                            googleMapResult.GOOGLE_RESULT_ORDER = (short)item.orderId;
                            googleMapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                            for (int i = 0; i < item.address.Count; i++)
                            {
                                if (i == 0)
                                    googleMapResult.ADDRESS_SEGMENT_01 = item.address[i].long_name;
                                else if (i == 1)
                                    googleMapResult.ADDRESS_SEGMENT_02 = item.address[i].long_name;
                                else if (i == 2)
                                    googleMapResult.ADDRESS_SEGMENT_03 = item.address[i].long_name;
                                else if (i == 3)
                                    googleMapResult.ADDRESS_SEGMENT_04 = item.address[i].long_name;
                                else if (i == 4)
                                    googleMapResult.ADDRESS_SEGMENT_05 = item.address[i].long_name;
                                else if (i == 5)
                                    googleMapResult.ADDRESS_SEGMENT_06 = item.address[i].long_name;
                                else if (i == 6)
                                    googleMapResult.ADDRESS_SEGMENT_07 = item.address[i].long_name;
                                else if (i == 7)
                                    googleMapResult.ADDRESS_SEGMENT_08 = item.address[i].long_name;
                                else if (i == 8)
                                    googleMapResult.ADDRESS_SEGMENT_09 = item.address[i].long_name;
                                else if (i == 9)
                                    googleMapResult.ADDRESS_SEGMENT_10 = item.address[i].long_name;

                            }

                            lstGoogleMapResult.Add(googleMapResult);
                        }

                    }


                    foreach (var item in lstGoogleMapResult)
                    {
                        var googleAddress = datacontext.GOOGLEMAPS_RESULTS.Where(x => x.ID_ADDRESS == item.ID_ADDRESS && (x.LATTITUDE == -1 || x.LONGITUD == -1)).SingleOrDefault();
                        if (googleAddress != null)
                        {
                            if (item.LATTITUDE != -1 || item.LONGITUD != -1)
                            {
                                googleAddress.LATTITUDE = item.LATTITUDE;
                                googleAddress.LONGITUD = item.LONGITUD;
                                googleAddress.NORMALIZED_ADDRESS = item.NORMALIZED_ADDRESS;
                                googleAddress.ADDRESS_SEGMENT_01 = item.ADDRESS_SEGMENT_01;
                                googleAddress.ADDRESS_SEGMENT_02 = item.ADDRESS_SEGMENT_02;
                                googleAddress.ADDRESS_SEGMENT_03 = item.ADDRESS_SEGMENT_03;
                                googleAddress.ADDRESS_SEGMENT_04 = item.ADDRESS_SEGMENT_04;
                                googleAddress.ADDRESS_SEGMENT_05 = item.ADDRESS_SEGMENT_05;
                                googleAddress.ADDRESS_SEGMENT_06 = item.ADDRESS_SEGMENT_06;
                                googleAddress.ADDRESS_SEGMENT_07 = item.ADDRESS_SEGMENT_07;
                                googleAddress.ADDRESS_SEGMENT_08 = item.ADDRESS_SEGMENT_08;
                                googleAddress.ADDRESS_SEGMENT_09 = item.ADDRESS_SEGMENT_09;
                                googleAddress.ADDRESS_SEGMENT_10 = item.ADDRESS_SEGMENT_10;
                                googleAddress.GOOGLE_RESULT_ORDER = item.GOOGLE_RESULT_ORDER;
                                datacontext.Entry(googleAddress).State = System.Data.Entity.EntityState.Modified;

                                datacontext.SaveChanges();
                            }
                        }
                        else
                        {
                            GOOGLEMAPS_RESULTS googleAddress1 = new GOOGLEMAPS_RESULTS();
                            googleAddress1.ID_ADDRESS = item.ID_ADDRESS;
                            googleAddress1.NORMALIZED_ADDRESS = item.NORMALIZED_ADDRESS;
                            googleAddress1.LATTITUDE = item.LATTITUDE;
                            googleAddress1.LONGITUD = item.LONGITUD;
                            googleAddress1.NORMALIZED_ADDRESS = item.NORMALIZED_ADDRESS;
                            googleAddress1.GOOGLE_RESULT_ORDER = item.GOOGLE_RESULT_ORDER;
                            googleAddress1.ADDRESS_SEGMENT_01 = item.ADDRESS_SEGMENT_01;
                            googleAddress1.ADDRESS_SEGMENT_02 = item.ADDRESS_SEGMENT_02;
                            googleAddress1.ADDRESS_SEGMENT_03 = item.ADDRESS_SEGMENT_03;
                            googleAddress1.ADDRESS_SEGMENT_04 = item.ADDRESS_SEGMENT_04;
                            googleAddress1.ADDRESS_SEGMENT_05 = item.ADDRESS_SEGMENT_05;
                            googleAddress1.ADDRESS_SEGMENT_06 = item.ADDRESS_SEGMENT_06;
                            googleAddress1.ADDRESS_SEGMENT_07 = item.ADDRESS_SEGMENT_07;
                            googleAddress1.ADDRESS_SEGMENT_08 = item.ADDRESS_SEGMENT_08;
                            googleAddress1.ADDRESS_SEGMENT_09 = item.ADDRESS_SEGMENT_09;
                            googleAddress1.ADDRESS_SEGMENT_10 = item.ADDRESS_SEGMENT_10;
                            datacontext.GOOGLEMAPS_RESULTS.Add(googleAddress1);
                            datacontext.SaveChanges();


                        }
                    }
                    // lstGoogleMapResult = lstGoogleMapResult.GroupBy(x => x.ID_ADDRESS).ToList();
                    //lstGoogleMapResult = lstGoogleMapResult.Distinct(x=>x.).ToList();

                    //var distinct = lstGoogleMapResult.Distinct(x => x.ID_ADDRESS);

                    var distinct = lstGoogleMapResult.GroupBy(x => x.ID_ADDRESS, (key, group) => group.First());

                    foreach (var item in distinct)
                    {
                        var query = from gmr in datacontext.GOOGLEMAPS_RESULTS
                                    from bmr in datacontext.BINGMAPS_RESULTS
                                    where gmr.ID_ADDRESS == bmr.ID_ADDRESS && gmr.ID_ADDRESS == item.ID_ADDRESS
                                    orderby gmr.ID_ADDRESS

                                    select new
                                    {
                                        AddressId = gmr.ID_ADDRESS,
                                        GoogleLatitude = gmr.LATTITUDE,
                                        GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
                                        GoogleLongitude = gmr.LONGITUD,
                                        BingLatitude = bmr.LATTITUDE,
                                        BingLongitude = bmr.LONGITUD,
                                        BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
                                        BingResultOrder = bmr.BING_RESULT_ORDER,
                                        GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER,
                                        BingId = bmr.BingId,
                                        GoogleId = gmr.GoogleId
                                    };

                        var existingcoordinatesComparison = datacontext.COORDINATES_COMPARISON
                                                            .Where(x => x.ID_ADDRESS == item.ID_ADDRESS)
                                                            .ToList();

                        datacontext.COORDINATES_COMPARISON.RemoveRange(existingcoordinatesComparison);

                        datacontext.SaveChanges();

                        var googleresultCount = datacontext.GOOGLEMAPS_RESULTS
                                                .Where(x => x.ID_ADDRESS == item.ID_ADDRESS && (x.LATTITUDE != -1 && x.LONGITUD != -1))
                                                .Count();

                        

                      

                        List<COORDINATES_COMPARISON> lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();
                        foreach (var res in query)
                        {


                            COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
                            coordinatesComparison.ID_ADDRESS = res.AddressId;
                            coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
                            coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
                            coordinatesComparison.GoogleId = res.GoogleId;
                            coordinatesComparison.BingId = res.BingId;
                            coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == -1 || res.BingLongitude == -1 || res.GoogleLatitude == -1 || res.GoogleLongitude == -1) ? -1 :
                                (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
                            lstCoordinatesComparison.Add(coordinatesComparison);
                        }

                        datacontext.COORDINATES_COMPARISON.AddRange(lstCoordinatesComparison);

                        if (googleresultCount > 0)
                        {
                            var addressResultCountList = datacontext.ADDRESS_RESULT_COUNT.Where(x => x.ID_ADDRESS == item.ID_ADDRESS).SingleOrDefault();
                            if (addressResultCountList != null)
                            {
                                addressResultCountList.GOOGLE_RESULT_COUNT = googleresultCount;
                                datacontext.Entry(addressResultCountList).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                        datacontext.SaveChanges();

                       
                    }//for loop

                    failedGoogleAddresses = (from add in datacontext.ADDRESSES
                                           join gadd in datacontext.GOOGLEMAPS_RESULTS
                                          on add.ID_ADDRESS equals gadd.ID_ADDRESS
                                           where
                                           gadd.LATTITUDE == -1 || gadd.LONGITUD == -1
                                           select new addressmodel
                                           {
                                               Id = add.ID_ADDRESS,
                                               Name = add.ADDRESS_FIELD01,
                                               Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                           }).Distinct().ToList();
                    resultcount = datacontext.ADDRESS_RESULT_COUNT.Count();

                }
                Tuple<int, List<addressmodel>> data = new Tuple<int, List<addressmodel>>(resultcount, failedGoogleAddresses.ToList());

                return Ok(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}