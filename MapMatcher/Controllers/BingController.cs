﻿


using MapMatcher.Models;
using Newtonsoft.Json;
using static MapMatcher.Util.DistanceFinder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;



namespace MapMatcher.Controllers
{
    public class BingController : ApiController
    {

        [HttpGet]
        public IHttpActionResult Get(dynamic coords)
        {
            var lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();

            //var bingCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.bing.Value);
            // var googleCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.google.Value);

            using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
            {
                //existingcoordinatesComparison = await datacontext.COORDINATES_COMPARISON
                //                                         .Where(x => x.ID_ADDRESS == id)
                //                                         .ToListAsync();

                var query = from gmr in datacontext.GOOGLEMAPS_RESULTS
                            from bmr in datacontext.BINGMAPS_RESULTS
                            where gmr.ID_ADDRESS == bmr.ID_ADDRESS && bmr.ID_ADDRESS == 1
                            orderby gmr.ID_ADDRESS

                            select new
                            {
                                AddressId = gmr.ID_ADDRESS,
                                GoogleLatitude = gmr.LATTITUDE,
                                GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
                                GoogleLongitude = gmr.LONGITUD,
                                BingLatitude = bmr.LATTITUDE,
                                BingLongitude = bmr.LONGITUD,
                                BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
                                BingResultOrder = bmr.BING_RESULT_ORDER,
                                GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER
                            };
                lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();
                foreach (var res in query)
                {


                    COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
                    coordinatesComparison.ID_ADDRESS = res.AddressId;
                    coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
                    coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
                    coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == -1 || res.BingLongitude == -1 || res.GoogleLatitude == -1 || res.GoogleLongitude == -1) ? -1 :
                        (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
                    lstCoordinatesComparison.Add(coordinatesComparison);
                }

            }
            return Ok(lstCoordinatesComparison);
        }

        [HttpPost]
        public IHttpActionResult Post(dynamic coords)
        {
            int resultcount = 0;
            var failedBingAddresses = new List<addressmodel>();

            try
            {
                var bingCoords = JsonConvert.DeserializeObject<List<Coord>>(coords.bing.Value);
                List<BINGMAPS_RESULTS> lstBingMapResult = new List<BINGMAPS_RESULTS>();
                using (MapMatcher.AddressCompareEntities datacontext = new MapMatcher.AddressCompareEntities())
                {
                    foreach (var item in bingCoords)
                    {
                        if (item.id != 0)
                        {
                            BINGMAPS_RESULTS bingMapResult = new BINGMAPS_RESULTS();
                            bingMapResult.ID_ADDRESS = item.id;
                            bingMapResult.LATTITUDE = item.lat;
                            bingMapResult.LONGITUD = item.lng;
                            bingMapResult.BING_RESULT_ORDER = (short)item.orderId;
                            bingMapResult.NORMALIZED_ADDRESS = item.normalizedAddress;
                            lstBingMapResult.Add(bingMapResult);
                        }

                    }


                    foreach (var item in lstBingMapResult)
                    {
                        var bingAddress = datacontext.BINGMAPS_RESULTS.Where(x => x.ID_ADDRESS == item.ID_ADDRESS && (x.LATTITUDE == -1 || x.LONGITUD == -1)).SingleOrDefault();
                        if (bingAddress != null)
                        {
                            if (item.LATTITUDE != -1 || item.LONGITUD != -1)
                            {
                                bingAddress.LATTITUDE = item.LATTITUDE;
                                bingAddress.LONGITUD = item.LONGITUD;
                                bingAddress.NORMALIZED_ADDRESS = item.NORMALIZED_ADDRESS;
                                bingAddress.ADDRESS_SEGMENT_01 = item.ADDRESS_SEGMENT_01;
                                bingAddress.ADDRESS_SEGMENT_02 = item.ADDRESS_SEGMENT_02;
                                bingAddress.ADDRESS_SEGMENT_03 = item.ADDRESS_SEGMENT_03;
                                bingAddress.ADDRESS_SEGMENT_04 = item.ADDRESS_SEGMENT_04;
                                bingAddress.ADDRESS_SEGMENT_05 = item.ADDRESS_SEGMENT_05;
                                bingAddress.ADDRESS_SEGMENT_06 = item.ADDRESS_SEGMENT_06;
                                bingAddress.ADDRESS_SEGMENT_07 = item.ADDRESS_SEGMENT_07;
                                bingAddress.ADDRESS_SEGMENT_08 = item.ADDRESS_SEGMENT_08;
                                bingAddress.ADDRESS_SEGMENT_09 = item.ADDRESS_SEGMENT_09;
                                bingAddress.ADDRESS_SEGMENT_10 = item.ADDRESS_SEGMENT_10;
                                bingAddress.BING_RESULT_ORDER = item.BING_RESULT_ORDER;
                                datacontext.Entry(bingAddress).State = System.Data.Entity.EntityState.Modified;

                                datacontext.SaveChanges();
                            }
                        }
                        else
                        {
                            BINGMAPS_RESULTS bingAddress1 = new BINGMAPS_RESULTS();
                            bingAddress1.ID_ADDRESS = item.ID_ADDRESS;
                            bingAddress1.NORMALIZED_ADDRESS = item.NORMALIZED_ADDRESS;
                            bingAddress1.LATTITUDE = item.LATTITUDE;
                            bingAddress1.LONGITUD = item.LONGITUD;
                            bingAddress1.BING_RESULT_ORDER = item.BING_RESULT_ORDER;

                            datacontext.BINGMAPS_RESULTS.Add(bingAddress1);
                            datacontext.SaveChanges();


                        }
                    }

                    var distinct = lstBingMapResult.GroupBy(x => x.ID_ADDRESS, (key, group) => group.First());

                    foreach (var item in distinct)
                    {
                        var query = from gmr in datacontext.GOOGLEMAPS_RESULTS
                                    from bmr in datacontext.BINGMAPS_RESULTS
                                    where gmr.ID_ADDRESS == bmr.ID_ADDRESS && bmr.ID_ADDRESS == item.ID_ADDRESS
                                    orderby gmr.ID_ADDRESS

                                    select new
                                    {
                                        AddressId = gmr.ID_ADDRESS,
                                        GoogleLatitude = gmr.LATTITUDE,
                                        GoogleNormalizedAddress = gmr.NORMALIZED_ADDRESS,
                                        GoogleLongitude = gmr.LONGITUD,
                                        BingLatitude = bmr.LATTITUDE,
                                        BingLongitude = bmr.LONGITUD,
                                        BingNormalizedAddress = bmr.NORMALIZED_ADDRESS,
                                        BingResultOrder = bmr.BING_RESULT_ORDER,
                                        GoogleResultOrder = gmr.GOOGLE_RESULT_ORDER,
                                        BingId = bmr.BingId,
                                        GoogleId = gmr.GoogleId
                                    };

                        var existingcoordinatesComparison = datacontext.COORDINATES_COMPARISON
                                                            .Where(x => x.ID_ADDRESS == item.ID_ADDRESS)
                                                            .ToList();

                        datacontext.COORDINATES_COMPARISON.RemoveRange(existingcoordinatesComparison);

                        datacontext.SaveChanges();

                        var bingresultCount = datacontext.BINGMAPS_RESULTS
                                                .Where(x => x.ID_ADDRESS == item.ID_ADDRESS && (x.LATTITUDE != -1 && x.LONGITUD != -1))
                                                .Count();





                        List<COORDINATES_COMPARISON> lstCoordinatesComparison = new List<COORDINATES_COMPARISON>();
                        foreach (var res in query)
                        {


                            COORDINATES_COMPARISON coordinatesComparison = new COORDINATES_COMPARISON();
                            coordinatesComparison.ID_ADDRESS = res.AddressId;
                            coordinatesComparison.GOOGLE_RESULT_ORDER = res.GoogleResultOrder;
                            coordinatesComparison.BING_RESULT_ORDER = res.BingResultOrder;
                            coordinatesComparison.GoogleId = res.GoogleId;
                            coordinatesComparison.BingId = res.BingId;
                            coordinatesComparison.DISTANCE_BETWEENPOINTS_METERS = (res.BingLatitude == -1 || res.BingLongitude == -1 || res.GoogleLatitude == -1 || res.GoogleLongitude == -1) ? -1 :
                                (decimal)GetDistanceFromLatLonInMeters((double)res.GoogleLatitude, (double)res.GoogleLongitude, (double)res.BingLatitude, (double)res.BingLongitude);
                            lstCoordinatesComparison.Add(coordinatesComparison);
                        }

                        datacontext.COORDINATES_COMPARISON.AddRange(lstCoordinatesComparison);

                        if (bingresultCount > 0)
                        {
                            var addressResultCountList = datacontext.ADDRESS_RESULT_COUNT.Where(x => x.ID_ADDRESS == item.ID_ADDRESS).SingleOrDefault();
                            if (addressResultCountList != null)
                            {
                                addressResultCountList.BING_RESULT_COUNT = bingresultCount;
                                datacontext.Entry(addressResultCountList).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                        datacontext.SaveChanges();


                    }//for loop

                    failedBingAddresses = (from add in datacontext.ADDRESSES
                                         join gadd in datacontext.BINGMAPS_RESULTS
                                        on add.ID_ADDRESS equals gadd.ID_ADDRESS
                                         where
                                         gadd.LATTITUDE == -1 || gadd.LONGITUD == -1
                                         select new addressmodel
                                         {
                                             Id = add.ID_ADDRESS,
                                             Name = add.ADDRESS_FIELD01,
                                             Address = add.ADDRESS_FIELD01 + "," + add.ADDRESS_FIELD02 + "," + add.ADDRESS_FIELD03 + "," + add.ADDRESS_FIELD04 + "," + add.ADDRESS_FIELD05 + "," + add.ADDRESS_FIELD06 + "," + add.ADDRESS_FIELD07,

                                         }).Distinct().ToList();
                    resultcount = datacontext.ADDRESS_RESULT_COUNT.Count();

                }
                Tuple<int, List<addressmodel>> data = new Tuple<int, List<addressmodel>>(resultcount, failedBingAddresses.ToList());

                return Ok(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}