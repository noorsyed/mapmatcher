﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('failedList', failedList);

   // failedList.$inject = ['$scope', missingInfo];

    function failedList($scope, missingInfo, $modalInstance) {
        $scope.title = 'failedList';
        $scope.missingAddress = [];
        activate();

        function activate() {
            if (missingInfo) {
                $scope.missingAddress = missingInfo;
            }
           
        }
        $scope.close = function () {
            $modalInstance.close();
        }
    }
})();
