﻿(function () {
    'use strict';
    var controllerId = 'dashboard';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$scope', '$http', 'spinner', '$modal', dashboard]);

    function dashboard(common, datacontext, $scope, $http, spinner, $modal) {
        var getLogFn = common.logger.getLogFn;

        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');
        var logWarn = getLogFn(controllerId, 'warn');

        var vm = this;
        vm.comparisonResult = [];
        vm.message = '';
        vm.googleMessage = '';
        vm.bingMessage = '';
        vm.messageCount = 0;
        vm.people = [];
        vm.addresses = [];
        vm.failedGoogleAddresses = [];
        vm.failedBingAddresses = [];
        vm.title = 'Dashboard';
        vm.bingCoords = [];
        vm.bingLatitude = 0;
        vm.bingNormalizedAddress = '';
        vm.bingLongitude = 0;
        var map = null;
        var gmap = null;
        var searchManager = null;
        vm.selectedAddress = {};
        vm.loadSearchModule = function () {
            Microsoft.Maps.loadModule('Microsoft.Maps.Search', { callback: geocodeRequest })
        }

        vm.disableSubmit = false;

        vm.pagerData;
        vm.pageList = [];
        vm.ispaging = true;
        vm.mainpageId = 1;

        var isGetAddress = false;


        //var myLatLng = codeAddress();// {lat: -25.363, lng: 131.044};
        function initMap() {
            function addPushpin(coordinates) {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 4,
                    center: coordinates
                });

                var marker = new google.maps.Marker({
                    position: coordinates,
                    map: map,
                    title: 'Hello World!'
                });
            }
            codeAddress(addPushpin);
        }

        activate();

        function activate() {
            spinner.spinnerShow("Loading...");
            vm.message = '';
            vm.googleMessage = '';
            vm.bingMessage = '';
            vm.mainpageId = 1;
            vm.comparisonResult = [];

            map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                credentials: 'Au0xLCnttCaU-MxhbAbi2SIGw-KETBIlOJEPGej1A624CdkeFWzKTt7I8QMmizkD'
            });
            paging(vm.mainpageId);


        }
        vm.pagination = function (pageId) {
            paging(pageId);
        }
        function paging(pageId) {

            window.bingCoords = [];
            window.googleCoords = [];

            $http({

                method: 'GET', url: 'api/address' + '/' + pageId,

            })
            .success(function (result) {

                $scope.ispaging = false;

                vm.pagerData = result.m_Item1[0];

                if (result.m_Item3.length > 0) {
                    vm.googleMessage = result.m_Item3.length + ' Google addresses not processed';
                    vm.failedGoogleAddresses = result.m_Item3
                } else {
                    vm.googleMessage = '';
                }

                if (result.m_Item4.length > 0) {
                    vm.bingMessage = result.m_Item4.length + 'Bing addresses not processed';
                    vm.failedBingAddresses = result.m_Item4
                } else {
                    vm.bingMessage = '';
                }
                if (parseInt(vm.pagerData.NumberOfPages) > 1) {
                    $scope.ispaging = false;//true;

                    if (vm.mainpageId == 1) {
                        localStorage.setItem("TotalPages", vm.pagerData.NumberOfPages)
                    }
                }
                vm.pageList = [];
                
                if (result.m_Item2.length > 0) {
                    vm.addresses = result.m_Item2;
                    vm.loadSearchModule();
                    vm.selectedAddress = vm.addresses[0];
                    if (vm.mainpageId > 1) {
                        window.bingCoords = [];
                        window.googleCoords = [];
                        vm.locatePlaces();
                    }
                    else {
                        spinner.spinnerHide();
                    }
                }
                else {
                    spinner.spinnerHide();
                    vm.cssClass = "alert alert-success alert-dismissible";
                    vm.message = 'All addresses processed successfully';
                    //vm.googleMessage = '';
                    //vm.bingMessage = '';
                    logWarn("No locations.");
                }


            })
            .error(function (error) {
                logError(error);
            })
        }

        function createSearchManager() {
            map.addComponent('searchManager', new Microsoft.Maps.Search.SearchManager(map));
            searchManager = map.getComponent('searchManager');
        }

        function geocodeRequest() {
            createSearchManager();


            //if (vm.addresses.length > 0) {


            //    var userData = { id: vm.addresses[0].Id };
            //    var request = {
            //        where: vm.addresses[0].Address,
            //        count: 5,
            //        bounds: map.getBounds(),
            //        callback: onGeocodeSuccess,
            //        errorCallback: onGeocodeFailed,
            //        userData: userData
            //    };

            //    searchManager.geocode(request);

            //    googleMap({ id: vm.addresses[0].Id, address: vm.addresses[0].Address });
            //}
        }

        function onGeocodeSuccess(result, userData) {

            if (result) {
                map.entities.clear();
                vm.bingCoords = [];
                vm.comparisonResult = [];
                var topResult = result.results && result.results[0];
            

                if (result.results.length > 0) {

                    var i = 0;
                    angular.forEach(result.results, function (value) {

                        var pushpin = new Microsoft.Maps.Pushpin(value.location, null);
                        window.bingCoords.push({
                            lat: value.location.latitude,
                            lng: value.location.longitude,
                            id: userData.id,
                            normalizedAddress: value.name,
                            orderId: ++i

                        });
                        $scope.$apply(function () {
                            vm.bingCoords.push({
                                lat: value.location.latitude,
                                lng: value.location.longitude,
                                normalizedAddress: value.name
                            });
                            //vm.bingLatitude = value.location.latitude;
                            //vm.bingLongitude = value.location.longitude;
                            //vm.bingNormalizedAddress = value.name;
                        })
                        map.setView({ center: value.location, zoom: 8 });
                        map.entities.push(pushpin);
                    })

                    if (isGetAddress) {
                        isGetAddress = false;
                        $http({
                            method: 'Post',
                            url: 'api/comparison',
                            data: {
                                bing: JSON.stringify(window.bingCoords),
                                google: JSON.stringify(window.googleCoords),

                            }
                        })
                       .success(function (res) {
                           vm.comparisonResult = res;
                           isGetAddress = false;
                       })
                       .error(function (err) {
                           logError(err);
                           isGetAddress = false;
                       })
                    }


                    //var myLatlng = new google.maps.LatLng(topResult.location.latitude, topResult.location.longitude);
                    //var myOptions = {
                    //    zoom: 10,
                    //    center: myLatlng,
                    //    mapTypeId: google.maps.MapTypeId.ROADMAP
                    //}

                    ////var img_url = "http://maps.googleapis.com/maps/api/staticmap?center=" + myLatlng + "&zoom=14&size=400x400&sensor=false";
                    ////document.getElementById("map").innerHTML = "<img src='" + img_url + "'>";
                    //gmap = new google.maps.Map(document.getElementById("map"), myOptions);
                    //// marker refers to a global variable
                    //var marker = new google.maps.Marker({
                    //    position: myLatlng,
                    //    map: gmap
                    //});
                }
                else {
                    $scope.$apply(function () {
                        vm.bingCoords = [];
                        vm.comparisonResult = [];
                        vm.bingLatitude = '';
                        vm.bingLongitude = '';
                        vm.bingNormalizedAddress = '';
                    })

                }
            }
        }
        function onGeocodeFailed(result, userData) {
            logError('Geocode failed');
        }



        vm.getaddress = function () {
            document.getElementById("googleCoords").innerHTML = '';
            isGetAddress = true;
           // document.getElementById("comResult").innerHTML = '';
            vm.comparisonResult = [];
            if (typeof vm.selectedAddress == "object") {


                window.bingCoords = [];
                window.googleCoords = [];
                if (!searchManager) {
                    vm.loadSearchModule();
                }

                var userData = { id: vm.id };
                var request = {
                    where: vm.selectedAddress.Address,
                    count: 5,
                    bounds: map.getBounds(),
                    callback: onGeocodeSuccess,
                    errorCallback: onGeocodeFailed,
                    userData: userData
                };

                googleMap({ id: vm.selectedAddress.Id, address: vm.selectedAddress.Address })
                    .then(res=> {
                        searchManager.geocode(request);
                    }, err=> {
                        logError(err);
                    });


            }
            else {
                log("No valid address selected")
            }
        }


        var locatePlace = function (addr) {

            return function () {

                var userData = { id: addr.Id };
                var request = {
                    where: addr.Address,
                    bounds: map.getBounds(),
                    callback: onGeocodeSuccess,
                    errorCallback: onGeocodeFailed,
                    userData: userData
                };

                if (searchManager) {
                    searchManager.geocode(request);
                }
                else {
                    vm.loadSearchModule();
                }
                googleMap({ id: addr.Id, address: addr.Address }).then((res) => {
                    if (vm.addresses.length - 1 == vm.addresses.indexOf(addr)) {

                        var tPages = localStorage.getItem("TotalPages")

                        if (vm.mainpageId <= tPages) {
                            //spinner.spinnerShow();
                            vm.submit();
                            //vm.mainpageId = vm.mainpageId + 1;
                            //paging(vm.mainpageId);
                            //vm.locatePlaces();
                        }
                        else {
                            vm.disableSubmit = true;
                            //spinner.spinnerHide();
                            //logWarn("All locations are recorded, please click on submit to proceed further.");
                            log("Results submitted successfully!")
                        }
                    }
                }, (err) => {


                });

            }
        }

        vm.locatePlaces = function () {

            vm.disableSubmit = false;
            vm.message = '';

            //if (vm.mainpageId == 1) {
            window.bingCoords = [];
            window.googleCoords = [];
            // }
            var count = 1000;



            if (vm.addresses.length > 0) {
                if (vm.mainpageId == 1) {
                    spinner.spinnerShow("Loading...");
                }
                angular.forEach(vm.addresses, function (addr, key) {

                    setTimeout(locatePlace(addr), count)
                    count += 1000;
                })
            } else {
                vm.dismissAlert = true;
                vm.cssClass = "alert alert-info alert-dismissible";
                vm.message = 'Addresses have already been located.';
                logWarn("Addresses have already been located.");
            }



        }

        vm.getAddresses = function (viewValue) {
            //var params = { address: viewValue, sensor: false };
            //return $http.get('http://maps.googleapis.com/maps/api/geocode/json', { params: params })
            //.then(function (res) {
            //    return res.data.results;
            //});
            if (viewValue && viewValue.length > 2) {

                return $http.get('api/locate/' + viewValue)//, { id: viewValue })
                            .then(function (res) {
                                return res.data;
                            }, (err) => {
                                
                            });

            }
        };


        vm.submit = function () {
            var submitedPages = 0
            // spinner.spinnerShow();
            $http({
                method: 'POST',
                url: 'api/address',
                data: {
                    bing: JSON.stringify(window.bingCoords),
                    google: JSON.stringify(window.googleCoords),
                    pageId: vm.mainpageId,
                    nextPage: vm.pagerData.HasNextPage
                }
            })

            .success(function (res) {
                var tPages = localStorage.getItem("TotalPages")

                //vm.disableSubmit = true;
                //spinner.spinnerHide();
                //window.bingCoords = [];
                //window.googleCoords = [];
                //log("Results submitted successfully!")

                if (vm.mainpageId < tPages) {
                    submitedPages = vm.mainpageId * 10;
                    vm.mainpageId = vm.mainpageId + 1;
                    //log(submitedPages + " Results submitted successfully!");
                    // log("Successfully processed " + submitedPages + "... records");

                    spinner.spinnerShow("Processed " + res.m_Item1 + " records");
                    window.bingCoords = [];
                    window.googleCoords = [];
                    paging(vm.mainpageId);
                }
                else {
                    spinner.spinnerHide();
                    vm.cssClass = "alert alert-success alert-dismissible";

                    vm.message = 'All addresses processed successfully';

                    if (res.m_Item2.length > 0) {
                        vm.googleMessage = res.m_Item2.length + ' Google addresses not processed';
                        vm.failedGoogleAddresses = res.m_Item2
                    } else {
                        vm.googleMessage = '';
                    }

                    if (res.m_Item3.length > 0) {
                        vm.bingMessage = res.m_Item3.length + 'Bing addresses not processed';
                        vm.failedBingAddresses = res.m_Item3
                    } else {
                        vm.bingMessage = '';
                    }




                    log("All addresses processed successfully");
                    window.bingCoords = [];
                    window.googleCoords = [];
                    vm.disableSubmit = true;
                    vm.addresses = {};
                }

            })
            .error(function (err) {
                //vm.disableSubmit = true;
                submitedPages = vm.mainpageId * 10;
                spinner.spinnerHide();
                vm.message = 'Processing failed, after' + err.ExceptionMessage;
                vm.cssClass = "alert alert-danger alert-dismissible";
                logError("Processing failed, after" + err.ExceptionMessage + " records!")
                //activate();
            })
        }

        vm.hideAlert = function () {
            vm.dismissAlert = false;
        };



        var googleLocatePlace = function (addr) {
            return function () {
                googleMap({ id: addr.Id, address: addr.Address }).then((res) => {
                    if (vm.failedGoogleAddresses.length - 1 == vm.failedGoogleAddresses.indexOf(addr)) {
                        vm.googleSubmit();
                    }
                }, (err) => {

                });
            }
        }



        vm.gooleProcessFailedAddresses = function () {

            window.googleCoords = [];

            var count = 1000;
            spinner.spinnerShow("Processing...");


            angular.forEach(vm.failedGoogleAddresses, function (addr) {

                setTimeout(googleLocatePlace(addr), count)
                count += 1000;
            });


        }

        var bingLocatePlace = function (addr) {
            return function () {

                var userData = { id: addr.Id };
                var request = {
                    where: addr.Address,
                    bounds: map.getBounds(),
                    callback: onGeocodeSuccess,
                    errorCallback: onGeocodeFailed,
                    userData: userData
                };
                if (searchManager) {

                    searchManager.geocode(request);
                    if (vm.failedBingAddresses.length - 1 == vm.failedBingAddresses.indexOf(addr)) {
                        vm.bingSubmit();

                    }
                }
                else {
                    vm.loadSearchModule();
                }

            }
        }


        vm.bingProcessFailedAddresses = function () {

            window.BingCoords = [];

            var count = 1000;
            spinner.spinnerShow("Processing...");

            angular.forEach(vm.failedBingAddresses, function (addr) {

                setTimeout(bingLocatePlace(addr), count)
                count += 1000;
            })


        };

        vm.googleSubmit = function () {

            $http({
                method: 'POST',
                url: 'api/locate',
                data: {
                    google: JSON.stringify(window.googleCoords)
                }
            })

           .success(function (res) {
               spinner.spinnerHide();

               if (res.m_Item2.length > 0) {
                   vm.message = "Processed all failed google addresses except " + res.m_Item2.length;
                   vm.googleMessage = res.m_Item2.length + ' Google addresses not processed';
                   vm.failedGoogleAddresses = res.m_Item2;
                   vm.cssClass = "alert alert-success alert-dismissible";
               } else {
                   vm.googleMessage = '';
                   vm.failedGoogleAddresses = '';
                   vm.message = "Processed all failed google addresses";
                   vm.cssClass = "alert alert-success alert-dismissible";
               }

               log("Processed all failed google addresses except " + res.m_Item2.length);

               window.googleCoords = [];
           })
           .error(function (err) {

               spinner.spinnerHide();
               logError("Processing failed" + err.ExceptionMessage + " !")

           })

        }

        vm.bingSubmit = function () {

            $http({
                method: 'POST',
                url: 'api/bing',
                data: {
                    bing: JSON.stringify(window.bingCoords)
                }
            })

           .success(function (res) {
               spinner.spinnerHide();

               if (res.m_Item2.length > 0) {
                   vm.bingMessage = res.m_Item2.length + ' Bing addresses not processed';
                   vm.failedBingAddresses = res.m_Item2;
                   vm.message = "Processed all failed bing addresses except " + res.m_Item2.length;
                   vm.cssClass = "alert alert-success alert-dismissible";
               } else {
                   vm.bingMessage = '';
                   vm.failedBingAddresses = '';
                   vm.message = '';
                   vm.cssClass = '';
               }


               log("Processed all failed bing addresses except " + res.m_Item2.length);
               window.bingCoords = [];

           })
           .error(function (err) {

               spinner.spinnerHide();
               logError("Processing failed" + err.ExceptionMessage + " !")

           })

        }

        vm.clear = function () {
            var modalInstance = $modal.open({
                templateUrl: '/app/dashboard/confirmationDialog.html',
                controller: 'confirmationDialogCtrl',
                backdrop: 'static',

            });

            modalInstance.result.then(function () {
                spinner.spinnerShow("Please wait...");

                $http({
                    method: 'DELETE',
                    url: 'api/address'
                })
               .success(function (result) {
                   // vm.disableSubmit = false;
                   vm.comparisonResult = [];
                   document.getElementById("googleCoords").innerHTML = '';
                   spinner.spinnerHide();
                   activate();
                   log("Data deleted successfully");

               })
               .error(function (error) {
                   spinner.spinnerHide();
                   logError(error);
               })
            }, function () {

            });
        }

        vm.showBingMessage = function () {

            var modalInstance = $modal.open({
                templateUrl: '/app/dashboard/failedList.html',
                controller: 'failedList',
                backdrop: 'static',
                resolve: {
                    missingInfo: function () {
                        return vm.failedBingAddresses;
                    }
                }

            });


        }


        vm.showGoogleMessage = function () {
            var modalInstance = $modal.open({
                templateUrl: '/app/dashboard/failedList.html',
                controller: 'failedList',
                backdrop: 'static',
                resolve: {
                    missingInfo: function () {
                        return vm.failedGoogleAddresses;
                    }
                }

            });
        }
    }
})();